

Various Gradle Plugins
======================

This project was born out of of having to do various things in Gradle for which support was not
yet available. Typically this would have been where usage of Gradle was not mainstream or new
technology that came along was not yet available in Gradle.

- [Gnu Make Gradle Plugin](./gnumake) : Allows [GNU Make](https://www.gnu.org/software/make) to be called from within a Gradle script 
- [Doxygen Gradle Plugin](./doxygeb) : Allows documentation to be generated using [Doxygen](http://www.doxygen.org) 
- [Bintray Gradle Plugin](https://github.com/ysb33r/bintray) : The Bintray plugin now lives in its own [repository](https://github.com/ysb33r/bintray)

